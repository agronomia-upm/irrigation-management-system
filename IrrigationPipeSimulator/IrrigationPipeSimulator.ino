#include <SoftwareSerial.h>
#include "DataWarehouse.h"


SoftwareSerial sw = SoftwareSerial(2, 3); // RX, TX
DataWarehouse  *data   = new DataWarehouse();

float getRandom() {
  return random(1, 100) / 100.0 ;
}

void setup() {
  Serial.begin(9600);
  Serial.println("Starting!!!!");
  sw.begin(9600);
}

void loop() {
  Serial.println("Generating random values.");

  data->set(DATAWAREHOUSE_VALUES::MANOMETER_1, 3 + getRandom());
  data->set(DATAWAREHOUSE_VALUES::MANOMETER_2, 3 + getRandom());
  data->set(DATAWAREHOUSE_VALUES::MANOMETER_3, 3 + getRandom());
  data->set(DATAWAREHOUSE_VALUES::FLOWMETER, 12 + getRandom());
  data->set(DATAWAREHOUSE_VALUES::TEMPERATURE, 25 + getRandom());
  data->set(DATAWAREHOUSE_VALUES::HUMIDITY, 82 + getRandom());

  Serial.println(data->serialize());
  sw.print(data->serialize());
  
  delay(1000);
}
