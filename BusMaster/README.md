# Overview

# Irrigation Management System

En la actualidad se están estableciendo huertos urbanos los cuáles adolecen de sistemas de
automatización para la gestión de recursos. Dado el bajo coste que la sensórica está alcanzando,
se están desarrollando sistemas aplicados a este nuevo concepto en los que la
tecnología aplicada está marcando un papel primordial.

Por este motivo se ha desarrollado un modelo de automatización en sistemas de riego localizado
para pequeñas superficies. Dicho sistema consta de sensores de temperatura, humedad relativa,
presión y caudal, así como de un actuador, como es el caso de la electroválvula, todo ello
controlado por placas Arduino.

Al tener una finalidad divulgativa, se ha instalado un aparto de medida analógico en paralelo
por cada uno de los sensores digitales, tanto de presión como de caudal, de tal forma que se
pueda comparar con relativa facilidad las medidas analógicas y digitales en cada uno ellos.

![Alt text](schema-small.png?raw=true "Esquema general")
