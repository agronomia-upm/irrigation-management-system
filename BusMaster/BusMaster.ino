#include "Arduino.h"
#include <Wire.h>


#include "DataWarehouse.h"
#include "PlatformHandler.h"

#include "ScreenHandler.h"
#include "DHTHandler.h"
#include "BusHandler.h"
#include "BusMaster.h"


#define DHTPIN          51 // Pin for DHT sensor
#define RX              19 // RX pin for serial communication with IoT platform forwarder 
#define TX              18 // TX pin for serial communication with IoT platform forwarder


DataWarehouse   *data     = new DataWarehouse();
ScreenHandler   *screen   = new ScreenHandler(data);
DHTHandler      *dht      = new DHTHandler(DHTPIN, data);
BusHandler      *bus      = new BusHandler(data);
BusMaster       *master   = new BusMaster(bus, screen);
PlatformHandler *platform = new PlatformHandler(TX, RX, data);


void setup(void) {
  Serial.begin(9600);

  screen->begin();
  dht->begin();
  bus->begin();
  master->begin();
  platform->begin();
}

void loop(void) {
  screen->loop();
  dht->loop();
  bus->loop();
  master->loop();
  platform->loop();
}
