#include <Arduino.h>
#include <Wire.h>

#include "BusSlave.h"
#include "FlowHandler.h"
#include "PressureHandler.h"


#define FLOW_PIN   8
#define PRESS_PIN   A0

#define FLOW_ADDR 10

FlowHandler flowHandler = FlowHandler();
PressureHandler pressureHandler = PressureHandler(PRESS_PIN);

long nextMillis = 0;


/**
   function that executes whenever a pulse happends. 
   This function is registered as an event, see setup()
*/
void pulseEvent() {
  flowHandler.pulse();
}

/**
   function that executes whenever data is requested by master.
   This function is registered as an event, see setup()
*/
void requestEvent() {
  Serial.println("FlowController::requestEvent()");
  BusSlave::send(pressureHandler.getPressure(), flowHandler.getFrequency());
}


void setup() {
  Serial.begin(9600);
  Wire.begin(FLOW_ADDR);            // join i2c bus with address #8
  Wire.onRequest(requestEvent);     // register event

  attachInterrupt(digitalPinToInterrupt(FLOW_PIN), pulseEvent, RISING);
}


void loop() {
  flowHandler.loop();
  pressureHandler.loop();
  
  long currentMillis = millis();
  if (nextMillis < currentMillis) {
    nextMillis = (currentMillis + 1000);

    float frequency = flowHandler.getFrequency();
    Serial.println("FlowController::loop()::flow: " + String(flowHandler.getFrequency()));
    Serial.println("FlowController::loop()::press: " + String(pressureHandler.getPressure()));
  }
}
