#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>


#define WIFI_SSID                 "agronomia"
#define WIFI_PASSWORD             "4gr0n0m14"

#define SERVER_URL                "192.168.4.1"
#define TOKEN                     "UOveGvMCIDowNhn9aF2I"

#define RELAY_PIN                 D1
#define SOIL_MOISTURE_DATA_PIN    A0
#define SOIL_MOISTURE_POWER_PIN   D2

#define OPEN_VALVE                HIGH
#define CLOSED_VALVE              LOW


WiFiClient wifiClient;
PubSubClient client(wifiClient);

int status = WL_IDLE_STATUS;

//Electrovalve state
int valveStatus = CLOSED_VALVE;

void setup() {
  Serial.begin(115200);

  pinMode(RELAY_PIN, OUTPUT); //Set relayPin as an OUTPUT pin

  pinMode(SOIL_MOISTURE_POWER_PIN, OUTPUT);//Set soil power pin as an OUTPUT
  digitalWrite(SOIL_MOISTURE_POWER_PIN, LOW);//Set to LOW so no power is flowing through the sensor

  InitWiFi();
  client.setServer(SERVER_URL, 1883);
  client.setCallback(on_message);
}

int last_send = 0;
void loop() {
  if ( !client.connected() ) {
    reconnect();
  }

  int seconds = millis() / 1000;

  if (seconds > last_send) {
    last_send = seconds;
    //Serial.println("Sending info");
    client.publish("v1/devices/me/telemetry", getTelemetryPayload().c_str());
  }

  client.loop();
}

// =======================================

int readSoil() {
  digitalWrite(SOIL_MOISTURE_POWER_PIN, HIGH);
  delay(10);

  int value = analogRead(SOIL_MOISTURE_DATA_PIN);
  digitalWrite(SOIL_MOISTURE_POWER_PIN, LOW);
  return value;
}

// =======================================

void startWatering() {
  if (valveStatus == CLOSED_VALVE) {
    Serial.println("Start watering");
    setValveStatus(OPEN_VALVE);
  }
}

// =======================================

void stopWatering() {
  if (valveStatus == OPEN_VALVE) {
    Serial.println("Stop watering");
    setValveStatus(CLOSED_VALVE);
  }
}

// =======================================

void setValveStatus(int status) {
  if (valveStatus != status) {
    valveStatus = status;
    digitalWrite(RELAY_PIN, valveStatus);
    client.publish("v1/devices/me/attributes", getStatusPayload().c_str());
  }
}

// =======================================

String getStatusPayload() {
  // Prepare gpios JSON payload string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  //data["humidity"] = readSoil();
  data["valveStatus"] = (valveStatus == OPEN_VALVE);

  char payload[256];
  data.printTo(payload, sizeof(payload));

  String strPayload = String(payload);
  //Serial.print("Get gpio status: ");
  //Serial.println(strPayload);
  return strPayload;
}

String getTelemetryPayload() {
  // Prepare gpios JSON payload string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  data["humidity"] = readSoil();
  //data["valve"] = valve;

  char payload[256];
  data.printTo(payload, sizeof(payload));

  String strPayload = String(payload);
  //Serial.print("Get gpio status: ");
  //Serial.println(strPayload);
  return strPayload;
}

// =======================================

// The callback for when a PUBLISH message is received from the server.
void on_message(const char* topic, byte* payload, unsigned int length) {

  Serial.println("On message");

  char json[length + 1];
  strncpy (json, (char*)payload, length);
  json[length] = '\0';

  Serial.print("Topic: ");
  Serial.println(topic);

  Serial.print("Message: ");
  Serial.println(json);

  // Decode JSON request
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.parseObject((char*)json);

  if (!data.success())
  {
    Serial.println("parseObject() failed");
    return;
  }

  // Check request method
  String methodName = String((const char*)data["method"]);
  String params = String((const char*)data["params"]);

  Serial.print("Method: ");
  Serial.println(methodName);

  Serial.print("Params: ");
  Serial.println(params);

  if (methodName.equals("setValveStatus")) {
    String responseTopic = String(topic);
    responseTopic.replace("request", "response");

    if (params == "true") {
      Serial.print("Method: setValveStatus(true)");
      startWatering();
    }
    else if (params == "false") {
      Serial.print("Method: setValveStatus(false)");
      stopWatering();
    }

    client.publish(responseTopic.c_str(), getStatusPayload().c_str());
  }
}

// =======================================

void InitWiFi() {
  Serial.println("Connecting to AP ...");
  // attempt to connect to WiFi network

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}

// =======================================

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to ThingsBoard node ...");
    // Attempt to connect (clientId, username, password)
    if ( client.connect("ESP8266 Device", TOKEN, NULL) ) {
      Serial.println( "[DONE]" );
      // Subscribing to receive RPC requests
      client.subscribe("v1/devices/me/rpc/request/+");

      // Sending current GPIO status
      Serial.println("Sending current status ...");
      client.publish("v1/devices/me/attributes", getStatusPayload().c_str());
    } else {
      Serial.print( "[FAILED] [ rc = " );
      Serial.print( client.state() );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}
