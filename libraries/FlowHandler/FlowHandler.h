#ifndef FlowHandler_h
#define FlowHandler_h

#include "Arduino.h"
#include <util/atomic.h> // this library includes the ATOMIC_BLOCK macro.

class FlowHandler {
  public:
    void begin() { }

    void pulse() {
      ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        pulseConter++;
      }
    }

    float getFrequency() {
      ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        return frequency;
      }
    }

    void loop() {
      long currentMillis = millis();
      if (nextMillis < currentMillis) {
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
          // code with interrupts blocked (consecutive atomic operations will not get interrupted)
          frequency = pulseConter * (float(1000) / (interval + (currentMillis - nextMillis)));
          nextMillis = (currentMillis + interval);
          pulseConter = 0;
        }
      }
    }

  private:
    volatile int pulseConter = 0;
    float frequency = 0.0;
    const int interval = 500;
    long nextMillis = 0;
};

#endif
