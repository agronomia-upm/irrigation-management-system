#ifndef ValveController_h
#define ValveController_h

#include <Wire.h>
#include <Arduino.h>

#include "BusSlave.h"
#include "RelayHandler.h"


class ValveController: public BusSlave {
    public:
      ValveController() {}
      ValveController(int _address, int relayPin): BusSlave(_address) {
        relay = new RelayHandler(relayPin);
      }

      /**
      * function that executes whenever data is requested by master
      * this function is registered as an event, see setup()
      */
      virtual void requestEvent() {
        Serial.println("ValveController::requestEvent()");
        float state = relay->get();
        send(state);
      }

      /**
      * function that executes whenever data is received from master.
      * This function is registered as an event, see setup()
      */
      virtual void receiveEvent(int howMany) {
        Serial.println("ValveController::receiveEvent()");

        int x = read();    // receive byte as an integer
        switch (x) {
          case ValveStates::Open:
            relay->set(ValveStates::Open);
            Serial.println("BusSlave::receiveEvent(howMany)::OPEN");
            break;

          case ValveStates::Close:
            relay->set(ValveStates::Close);
            Serial.println("BusSlave::receiveEvent(howMany)::CLOSED");
            break;
        }
      }

      void begin() {
        BusSlave::begin();
        relay->begin();
      }

      void loop() {
        BusSlave::loop();
        relay->loop();

        long currentMillis = millis();
        if (nextMillis < currentMillis) {
          nextMillis = (currentMillis + 10000);

          Serial.println("ValveController::loop()::relay.toggle()");
          relay->toggle();
        }
      }

    private:
      RelayHandler *relay;
};

#endif
