#ifndef BusSlave_h
#define BusSlave_h

#include <Arduino.h>
#include <Wire.h>

class BusSlave {
    public:
      BusSlave() {}

      static void send(float value) {
        Serial.println("BusSlave::send(value)");

        char buff[7];
        dtostrf(value, 7, 2, buff);
        Wire.write(buff);
        Serial.println("BusSlave::send(value): " + String(buff));
      }

      static void send(float value1, float value2) {
        char buff[15];

        dtostrf(value1, 7, 2, buff);
        buff[7] = ';';
        dtostrf(value2, 7, 2, buff + 8);

        Wire.write(buff);
        Serial.println("BusSlave::send(value, value): " + String(buff));
      }

      static float read() {
        float val = Wire.read();    // receive byte as an integer
        Serial.println("BusSlave::read()" + String(val));
        return val;
      }
};

#endif
