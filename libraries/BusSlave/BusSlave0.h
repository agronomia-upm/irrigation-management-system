#ifndef BusSlave_h
#define BusSlave_h

#include <Arduino.h>
#include <Wire.h>

class BusSlave {
    public:
      BusSlave(int _address) {
        address = _address;
      }

      /**
      * function that executes whenever data is requested by master
      * this function is registered as an event, see setup()
      */
      virtual void requestEvent() {
        Serial.println("BusSlave::requestEvent()");
      }

      /**
      * function that executes whenever data is received from master.
      * This function is registered as an event, see setup()
      */
      virtual void receiveEvent(int howMany) {
        Serial.println("BusSlave::receiveEvent()");
      }

      void send(float value) {
        Serial.println("BusSlave::send(value)");

        char buff[7];
        dtostrf(value, 7, 2, buff);
        Wire.write(buff);
        Serial.println("BusSlave::send(value): " + String(buff));
      }

      void send(float value1, float value2) {
        char buff[15];

        dtostrf(value1, 7, 2, buff);
        buff[7] = ';';
        dtostrf(value2, 7, 2, buff + 8);

        Wire.write(buff);
        Serial.println("BusSlave::send(value, value): " + String(buff));
      }

      float read() {
        float val = Wire.read();    // receive byte as an integer
        Serial.println("BusSlave::read()" + String(val));
      }

      void begin() {
        Wire.begin(address);                // join i2c bus with address #8
      }

      void loop() {}

    protected:
      long nextMillis = 0;

    private:
      int address;
};

#endif
