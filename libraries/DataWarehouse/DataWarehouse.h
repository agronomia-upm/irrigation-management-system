#ifndef DataWarehouse_h
#define DataWarehouse_h

//#include "open_close_state.h"
#include "Arduino.h"
#include <ArduinoJson.h>

#define DATAWAREHOUSE_TYPE  float
#define DATAWAREHOUSE_SIZE  7

enum DATAWAREHOUSE_VALUES {VALVE, MANOMETER_1, MANOMETER_2, MANOMETER_3, FLOWMETER, TEMPERATURE, HUMIDITY};

class DataWarehouse {
  public:
    DATAWAREHOUSE_TYPE _values[DATAWAREHOUSE_SIZE];
    bool _dirties[DATAWAREHOUSE_SIZE];

    DataWarehouse() {
      for (int i = 0; i < DATAWAREHOUSE_SIZE; i++) {
        _values[i] = 0;
        _dirties[i] = false;
      }
    }

    void set(DATAWAREHOUSE_VALUES datumName, DATAWAREHOUSE_TYPE datumValue) {
      if ( _values[datumName] != datumValue) {
        Serial.println("DataWarehouse::set(" + toString(datumName) + "): " + String(_values[datumName]) + " => " + String(datumValue));
        _values[datumName] = datumValue;
        _dirties[datumName] = true;
      }
    }

    DATAWAREHOUSE_TYPE get(DATAWAREHOUSE_VALUES datumName) {
      _dirties[datumName] = false;
      return _values[datumName];
    }

    int dirty() {
      for (int i = DATAWAREHOUSE_VALUES::VALVE; i < DATAWAREHOUSE_SIZE; i++) {
        if (_dirties[i]) return true;
      }

      return false;
    }

    int dirty(DATAWAREHOUSE_VALUES datumName) {
      return _dirties[datumName];
    }

    String serialize() {
      StaticJsonDocument<300> doc;

      doc["valve"] = this->get(VALVE);
      doc["manometer1"] = this->get(MANOMETER_1);
      doc["manometer2"] = this->get(MANOMETER_2);
      doc["manometer3"] = this->get(MANOMETER_3);
      doc["flowmeter"]   = this->get(FLOWMETER);
      doc["temperature"] = this->get(TEMPERATURE);
      doc["humidity"]    = this->get(HUMIDITY);

      String output = "";
      serializeJson(doc, output);

      return output;
    }

  private:
    String toString(DATAWAREHOUSE_VALUES value) {
      switch (value) {
        case VALVE: return "VALVE";
        case MANOMETER_1: return "MANOMETER_1";
        case MANOMETER_2: return "MANOMETER_2";
        case MANOMETER_3: return "MANOMETER_3";
        case FLOWMETER: return "FLOWMETER";
        case TEMPERATURE: return "TEMPERATURE";
        case HUMIDITY: return "HUMIDITY";
      }

      return "ERROR";
    }
};

#endif
