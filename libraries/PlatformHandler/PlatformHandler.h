#ifndef PlatformHandler_h
#define PlatformHandler_h

#include "Arduino.h"
#include <SoftwareSerial.h>
#include "DataWarehouse.h"

#define LOOP_INTERVAL   2000


class PlatformHandler {
  public:
    PlatformHandler(uint8_t TX, uint8_t RX, DataWarehouse * _data) {
      this->data = _data;
      this->softwareSerial = new SoftwareSerial(RX, TX); // RX, TX
    }

    void begin() { 
      this->softwareSerial->begin(9600);
    }

    void loop() {
      long currentMillis = millis();
      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + LOOP_INTERVAL);
        this->softwareSerial->print(data->serialize());
      }
    }

  private:
    long nextMillis = 0;

    SoftwareSerial *softwareSerial;
    DataWarehouse  *data ;
};

#endif
