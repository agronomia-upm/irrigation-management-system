#ifndef DHTHandler_h
#define DHTHandler_h

#include "Arduino.h"
#include "DataWarehouse.h"
#include "DHT.h"

#define DHTTYPE DHT22

class DHTHandler {

  public:
    DHTHandler(uint8_t _pinNumber, DataWarehouse * _data) {
      pinNumber = _pinNumber;
      data = _data;
    }

    void begin() {
      dht = new DHT(pinNumber, DHTTYPE);
    }

    void loop() {
      long currentMillis = millis();
      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + 500);

        float h = dht->readHumidity();    // Leemos la Humedad
        float t = dht->readTemperature(); // Leemos la temperatura en grados Celsius

        data->set(DATAWAREHOUSE_VALUES::HUMIDITY, isnan(h) ? 0 : h);
        data->set(DATAWAREHOUSE_VALUES::TEMPERATURE, isnan(t) ? 0 : t);
      }
    }

  private:
    int pinNumber;
    DataWarehouse *data;
    DHT *dht;

    long nextMillis = 0;
};

#endif
