#ifndef RelayHandler_h
#define RelayHandler_h

#define OPEN HIGH
#define CLOSED LOW

#include <Arduino.h>
#include "ValveStates.h"

class RelayHandler {
  public:
    RelayHandler(int _pinNumber) {
      pinNumber = _pinNumber;
    }

    void begin() {
      pinMode(pinNumber, OUTPUT);
    };

    void loop() {
      digitalWrite(pinNumber, (state ==ValveStates::Open ? OPEN : CLOSED));
    };

    void set(float _state) {
      state = _state;
    }

    void toggle() {
      state = (state == ValveStates::Open ? ValveStates::Close : ValveStates::Open);
    }

    ValveStates get() {
      return state;
    }

  private:
    int pinNumber;
    float state = ValveStates::Close;
};

#endif
