#ifndef PressureHandler_h
#define PressureHandler_h

#include "Arduino.h"
#include <util/atomic.h> // this library includes the ATOMIC_BLOCK macro.

class PressureHandler {
  public:
    PressureHandler(int _pin) {
      pin = _pin;
    }

    void begin() { }

    void loop() {
      long currentMillis = millis();
      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + interval);
        pressure = analogRead(pin);
      }
    }

    int getPressure() {
      return pressure;
    }

  private:
    int pin = 0;
    const int interval = 500;
    long nextMillis = 0;
    int pressure = 0;
};

#endif
