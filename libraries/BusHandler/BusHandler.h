#ifndef BusHandler_h
#define BusHandler_h

#include "Arduino.h"
#include "DataWarehouse.h"
#include "ValveStates.h"

#define VALVE_ADDR     8
#define FILTER_ADDR    9
#define FLOW_ADDR    10

enum BUS_HANDLER_ACTION {
  BUS_ACTION_VOID, BUS_ACTION_OPEN_VALVE, BUS_ACTION_CLOSE_VALVE
};

class BusHandler {
  public:
    BusHandler(DataWarehouse * _data) {
      data = _data;
    }

    void begin() {
      Wire.begin();        // join i2c bus (address optional for master)
    }

    void loop() {
      //Serial.println("BusHandler::loop()");
      long currentMillis = millis();
      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + 500);
        updateValveStatus();
        updateFilterStatus();
        updateFlowStatus();
      }
    }


    void updateValveStatus() {
      //Serial.println("BusHandler::updateValveStatus()");

      String dataString = "";

      Wire.requestFrom(VALVE_ADDR, 7);
      while (Wire.available()) {
        char c = Wire.read();
        dataString = dataString + c;
        //Serial.println("BusHandler::updateStatus()::Wire.read(): " + String(c));
      }

      if (dataString != "") {
        float valve = dataString.toFloat();
        //Serial.println("BusHandler::updateValveStatus()::received: " + String(valve));
        data->set(DATAWAREHOUSE_VALUES::VALVE, valve);
      }
    }

    void updateFilterStatus() {
      //Serial.println("BusHandler::updateFilterStatus()");
      String dataString = "";

      Wire.requestFrom(FILTER_ADDR, 15);

      while (Wire.available()) {
        char c = Wire.read();
        dataString = dataString + c;
        //Serial.println("BusHandler::updateStatus()::Wire.read(): " + String(c));
      }

      if (dataString != "") {
        //Serial.println("BusHandler::updateFilterStatus()::received: " + dataString);
        //Serial.println("BusHandler::updateFilterStatus()::received: " + dataString.substring(0, 7));
        //Serial.println("BusHandler::updateFilterStatus()::received: " + dataString.substring(8, 15));

        float manometer1 = dataString.substring(0, 7).toFloat();
        float manometer2 = dataString.substring(8, 15).toFloat();

        data->set(DATAWAREHOUSE_VALUES::MANOMETER_1, manometer1);
        data->set(DATAWAREHOUSE_VALUES::MANOMETER_2, manometer2);
      }
    }

    void updateFlowStatus() {
      //Serial.println("BusHandler::updateFlowStatus()");
      String dataString = "";

      Wire.requestFrom(FLOW_ADDR, 15);

      while (Wire.available()) {
        char c = Wire.read();
        dataString = dataString + c;
        //Serial.println("BusHandler::updateStatus()::Wire.read(): " + String(c));
      }

      if (dataString != "") {
        //Serial.println("BusHandler::updateFilterStatus()::received: " + dataString);
        //Serial.println("BusHandler::updateFilterStatus()::received: " + dataString.substring(0, 7));
        //Serial.println("BusHandler::updateFilterStatus()::received: " + dataString.substring(8, 15));

        float manometer = dataString.substring(0, 7).toFloat();
        float flow = dataString.substring(8, 15).toFloat();

        data->set(DATAWAREHOUSE_VALUES::MANOMETER_3, manometer);
        data->set(DATAWAREHOUSE_VALUES::FLOWMETER, flow);
      }
    }

    void sendAction(BUS_HANDLER_ACTION action) {
      Serial.println("BusHandler::sendAction(): " + String(action));
      Wire.beginTransmission(VALVE_ADDR); // transmit to device #VALVE_ADDR

      switch (action) {
        case BUS_ACTION_OPEN_VALVE:  Wire.write(ValveStates::Open); break;
        case BUS_ACTION_CLOSE_VALVE: Wire.write(ValveStates::Close); break;
      }
      Wire.endTransmission();    // stop transmitting
    }

  private:
    DataWarehouse * data;
    long nextMillis = 0;
    float valve = ValveStates::Close;

};

#endif
