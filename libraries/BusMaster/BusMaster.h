#ifndef BusMaster_h
#define BusMaster_h

class BusMaster {
  public:
    BusMaster(BusHandler * _bus, ScreenHandler * _screen) {
      bus = _bus;
      screen = _screen ;
    }

    void begin() {}

    void loop() {
      long currentMillis = millis();

      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + 100);

        ScreenHandlerAction action = screen->pendingAction();

        switch (action) {
          case SCREEN_ACTION_OPEN_VALVE:
            bus->sendAction(BUS_ACTION_OPEN_VALVE);
            break;

          case SCREEN_ACTION_CLOSE_VALVE:
            bus->sendAction(BUS_ACTION_CLOSE_VALVE);
            break;
        }
      }
    }

  private:
    BusHandler * bus;
    ScreenHandler *screen;

    long nextMillis = 0;
};

#endif
