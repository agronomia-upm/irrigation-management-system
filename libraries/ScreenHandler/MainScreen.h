#include "Arduino.h"

#ifndef MainScreen_h
#define MainScreen_h

#include "DataWarehouse.h"
#include "ValveStates.h"
#include "Screen.h"

class MainScreen: public Screen {
  public:
    MainScreen(DataWarehouse *data, CurrentTouchScreen *ts, BaseScreenHandler *handler): Screen(data, ts, handler) {}

    void redraw() {
      ts->clear();

      ts->title("Watering status");

      ts->printLine("Valve status:       ", data->get(VALVE) == ValveStates::Open ? "OPEN" : "CLOSED", 1);
      ts->printLine("Manometer 1 (BAR):  ", String(data->get(MANOMETER_1)), 2);
      ts->printLine("Manometer 2 (BAR):  ", String(data->get(MANOMETER_2)), 3);
      ts->printLine("Flow meter (l/min): ", String(data->get(FLOWMETER)), 4);
      ts->printLine("Manometer 3 (BAR):  ", String(data->get(MANOMETER_3)), 5);
      ts->printLine("temperature (ºC):   ", String(data->get(DATAWAREHOUSE_VALUES::TEMPERATURE)), 6);
      ts->printLine("humidity:           ", String(data->get(DATAWAREHOUSE_VALUES::HUMIDITY)), 7);
    }

    void refresh() {
      if (data->dirty(DATAWAREHOUSE_VALUES::VALVE)) {
        ts->printLine("Valve status:       ", data->get(VALVE) == ValveStates::Open ? "OPEN" : "CLOSED", 1);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::MANOMETER_1)) {
        ts->printLine("Manometer 1 (BAR):  ", String(data->get(MANOMETER_1)), 2);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::MANOMETER_2)) {
        ts->printLine("Manometer 2 (BAR):  ", String(data->get(MANOMETER_2)), 3);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::FLOWMETER)) {
        ts->printLine("Flow meter (l/min): ", String(data->get(FLOWMETER)), 4);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::MANOMETER_3)) {
        ts->printLine("Manometer 3 (BAR):  ", String(data->get(MANOMETER_3)), 5);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::TEMPERATURE)) {
        ts->printLine("temperature (ºC):   ", String(data->get(DATAWAREHOUSE_VALUES::TEMPERATURE)), 6);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::HUMIDITY)) {
        ts->printLine("humidity:           ", String(data->get(DATAWAREHOUSE_VALUES::HUMIDITY)), 7);
      }
    }

    virtual void click(int line) {
      Screen::click(line);
      Serial.println("MainScreen::click() line: " + String(line));
      switch (line) {
        case 1: handler->changeScreen(ScreenHandlerWindows::SCREEN_VALVE); break;

        case 2: handler->changeScreen(ScreenHandlerWindows::SCREEN_FILTER); break;
        case 3: handler->changeScreen(ScreenHandlerWindows::SCREEN_FILTER); break;

        case 4: handler->changeScreen(ScreenHandlerWindows::SCREEN_FLOW); break;
        case 5: handler->changeScreen(ScreenHandlerWindows::SCREEN_FLOW); break;

        case 6: handler->changeScreen(ScreenHandlerWindows::SCREEN_DHT); break;
        case 7: handler->changeScreen(ScreenHandlerWindows::SCREEN_DHT); break;
      }
    }
};

#endif
