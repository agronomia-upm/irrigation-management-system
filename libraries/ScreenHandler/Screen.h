#ifndef Screen_h
#define Screen_h

//#include "open_close_state.h"
#include "DataWarehouse.h"
#include "BaseScreenHandler.h"

#include "Arduino.h"

class Screen {
  public:
    Screen(DataWarehouse *_data, CurrentTouchScreen *_ts, BaseScreenHandler *_handler) {
      data = _data;
      ts = _ts;
      handler = _handler;
    }

    virtual void redraw() = 0;

    virtual void refresh() = 0;

    virtual void loop() {}

    virtual void click(int line) {
      Serial.println("Screen::click() line: " + String(line));
      switch (line) {
        case 0: handler->changeScreen(ScreenHandlerWindows::SCREEN_INDEX); break;
      }
    }

  protected:
    DataWarehouse *data;
    CurrentTouchScreen *ts;
    BaseScreenHandler *handler;

    long nextMillis = 0;
};

#endif
