/*
  CurrentTouchScreen.h - Library for managing TFT LCD Shield Touch Screen coordinates.
  Created by Samuel Barrado Aguirre.
  Released into the public domain.
*/
#ifndef BaseScreenHandler_h
#define BaseScreenHandler_h

#include "Arduino.h"
#include "DataWarehouse.h"

#define ScreenHandlerWindows_SIZE  7

// Convertir en clase
enum ScreenHandlerWindows {
    SCREEN_INDEX, SCREEN_VALVE, SCREEN_FILTER, SCREEN_FLOW, SCREEN_DHT
};

// Convertir en clase
enum ScreenHandlerAction {
  SCREEN_ACTION_VOID, SCREEN_ACTION_OPEN_VALVE, SCREEN_ACTION_CLOSE_VALVE
};

class BaseScreenHandler {
  public:
    virtual void changeScreen(ScreenHandlerWindows screen) {};

    virtual void setAction(ScreenHandlerAction _action) {
      action = _action;
    };

    ScreenHandlerAction pendingAction() {
      ScreenHandlerAction lastAction = action;
      action = SCREEN_ACTION_VOID;
      return lastAction;
    }

  private:
    ScreenHandlerAction action = SCREEN_ACTION_VOID;
    ScreenHandlerAction lastAction = SCREEN_ACTION_VOID;
};

#endif
