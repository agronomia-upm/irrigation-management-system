#include "Arduino.h"

#ifndef DHTScreen_h
#define DHTScreen_h

#include "DataWarehouse.h"

#include "Screen.h"

class DHTScreen: public Screen {
  public:
    DHTScreen(DataWarehouse *data, CurrentTouchScreen *ts, BaseScreenHandler *handler): Screen(data, ts, handler) {}

    virtual void loop() {
      long currentMillis = millis();

      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + 1000);
        DATAWAREHOUSE_TYPE h = data->get(DATAWAREHOUSE_VALUES::HUMIDITY);
        ts->plot(h / 100);
      }
    }

    void redraw() {
      ts->clear();
      ts->title("Atmosphere");
      ts->printLine("temperature (ºC):   ", String(data->get(DATAWAREHOUSE_VALUES::TEMPERATURE)), 1);
      ts->printLine("humidity:           ", String(data->get(DATAWAREHOUSE_VALUES::HUMIDITY)), 2);

      ts->axis();
    }

    void refresh() {
      if (data->dirty(DATAWAREHOUSE_VALUES::TEMPERATURE)) {
        ts->printLine("temperature (ºC):   ", String(data->get(DATAWAREHOUSE_VALUES::TEMPERATURE)), 1);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::HUMIDITY)) {
        ts->printLine("humidity:           ", String(data->get(DATAWAREHOUSE_VALUES::HUMIDITY)), 2);
      }
    }

  private:
    long nextMillis = 0;
};

#endif
