#include "Arduino.h"

#ifndef ValveScreen_h
#define ValveScreen_h

#include "ValveStates.h"
#include "DataWarehouse.h"

#include "Screen.h"

class ValveScreen: public Screen {
  public:
    ValveScreen(DataWarehouse *data, CurrentTouchScreen *ts, BaseScreenHandler *handler): Screen(data, ts, handler) {}

    void redraw() {
      ts->clear();
      ts->title("Electrovalve");
      ts->printLine("Valve status:       ", data->get(VALVE) == ValveStates::Open ? "OPEN" : "CLOSED", 1);

      ts->printLine("        OPEN VALVE",  "", 4);
      ts->printLine("        CLOSE VALVE", "", 6);
    }
    void refresh() {
      if (data->dirty(DATAWAREHOUSE_VALUES::VALVE)) {
        ts->printLine("Valve status:       ", data->get(VALVE) == ValveStates::Open ? "OPEN" : "CLOSED", 1);
      }
    }

    virtual void click(int line) {
      Screen::click(line);

      //ts->printLine("Open valve.",  "", 3);
      //ts->printLine("Flow meter (l/min): ", String(0), 4);

      Serial.println("ValveScreen::click() line: " + String(line));
      switch (line) {
        case 4:
          handler->setAction(ScreenHandlerAction::SCREEN_ACTION_OPEN_VALVE);
          ts->clearLine(4);
          ts->printLine("        OPEN VALVE",  "", 4);
          break;
        case 6: handler->setAction(ScreenHandlerAction::SCREEN_ACTION_CLOSE_VALVE);
          ts->clearLine(6);
          ts->printLine("        CLOSE VALVE",  "", 6);
          break;
      }
    }
};

#endif
