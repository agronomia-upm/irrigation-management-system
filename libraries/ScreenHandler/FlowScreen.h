#include "Arduino.h"

#ifndef FlowScreen_h
#define FlowScreen_h

#include "DataWarehouse.h"
#include "Screen.h"

class FlowScreen: public Screen {
  public:
    FlowScreen(DataWarehouse *data, CurrentTouchScreen *ts, BaseScreenHandler *handler): Screen(data, ts, handler) {}

    void redraw() {
      ts->clear();
      ts->title("Actual watering");
        ts->printLine("Flow meter (l/min): ", String(data->get(FLOWMETER)), 1);
        ts->printLine("Manometer (BAR):    ", String(data->get(MANOMETER_3)), 2);

        ts->axis();
    }

    void refresh() {
      if (data->dirty(DATAWAREHOUSE_VALUES::FLOWMETER)) {
        ts->printLine("Flow meter (l/min): ", String(data->get(FLOWMETER)), 1);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::MANOMETER_3)) {
        ts->printLine("Manometer (BAR):    ", String(data->get(MANOMETER_3)), 2);
      }
    }

    virtual void loop() {
      long currentMillis = millis();

      if (nextMillis < currentMillis) {
        nextMillis = (currentMillis + 1000);
        DATAWAREHOUSE_TYPE f = data->get(DATAWAREHOUSE_VALUES::FLOWMETER);
        ts->plot(f > 150 ? 1 : f / 150);
      }
    }
};

#endif
