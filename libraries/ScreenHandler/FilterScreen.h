#include "Arduino.h"

#ifndef FilterScreen_h
#define FilterScreen_h

#include "DataWarehouse.h"

#include "Screen.h"

class FilterScreen: public Screen {
  public:
    FilterScreen(DataWarehouse *data, CurrentTouchScreen *ts, BaseScreenHandler *handler): Screen(data, ts, handler) {}

    void redraw() {
      ts->clear();
      ts->title("Filter preassures");
      ts->printLine("Manometer 1 (BAR):  ", String(0), 2);
      ts->printLine("Manometer 2 (BAR):  ", String(0), 3);
    }
    
    void refresh() {
      if (data->dirty(DATAWAREHOUSE_VALUES::MANOMETER_1)) {
        ts->printLine("Manometer 1 (BAR):  ", String(data->get(MANOMETER_1)), 2);
      }

      if (data->dirty(DATAWAREHOUSE_VALUES::MANOMETER_2)) {
        ts->printLine("Manometer 2 (BAR):  ", String(data->get(MANOMETER_2)), 3);
      }
    }
};

#endif
