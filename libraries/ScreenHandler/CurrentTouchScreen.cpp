/*
  CurrentTouchScreen.cpp - Library for managing TFT LCD Shield Touch Screen coordinates.
  Created by Samuel Barrado Aguirre.
  Released into the public domain.
*/

#include "Arduino.h"
#include "CurrentTouchScreen.h"

#include <TouchScreen.h>

// Sizes and limits for the current "2.6/2.8" TFT LCD Shield"
#define X_SIZE 320
#define X_BOUND 160
#define X_ORIGIN 950

#define Y_SIZE 240
#define Y_BOUND 156
#define Y_ORIGIN 915

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

// X size & dimension of lines variable area.
#define LINE_X_START    235
#define LINE_X_HEIGHT    25


// This function changes coordinates system from readed touch screen coordinates. 
// From a 1024 based coordinates into a SCREEN_SIZE (X_SIZE, Y_SIZE) one.
// int value: read coodinate.
// int min: min read bound value over screen.
// int max: max read bound value over screen.
// int size: dimension size.
int clean (int value, int min, int  max, int size) {
  float range = max - min;

  if (value > max) {
    value = max;
  }
  else if (value < min) {
    value = min;
  }

  return (int) (size * ( (range - (value - min)) / range ));
}

void CurrentTouchScreen::begin() {
  tft.reset();

  uint16_t identifier = tft.readID();
  Serial.println("Screen identifier: " + String(identifier));

  tft.begin(identifier);
  tft.setRotation(3);

  tft.fillScreen(BLACK);
}

void CurrentTouchScreen::clear() {
  tft.fillScreen(BLACK);
}

void CurrentTouchScreen::loop() {
  TSPoint p = ts.getPoint();

  //restore the TFT control pins
  pinMode(YP, OUTPUT);
  pinMode(XM, OUTPUT);

  if (p.z > 0) {
    int x = clean(p.x, X_BOUND, X_ORIGIN, X_SIZE);
    int y = clean(p.y, Y_BOUND, Y_ORIGIN, Y_SIZE);

    this->x = x;
    this->y = y;

    this->clicked = true;
  }
}

void CurrentTouchScreen::title(String title) {
  tft.setCursor(MARGIN, MARGIN);
  tft.setTextColor(YELLOW);
  tft.setTextSize(3);
  tft.println(title);
}

void CurrentTouchScreen::axis() {
  tft.drawLine(0, Y_SIZE - 20, 320, Y_SIZE - 20, WHITE);
  tft.drawLine(0, Y_SIZE - 20, 0, Y_SIZE - 100, WHITE);
}

int CurrentTouchScreen::getLine() {
  int y_coordinate = this->y;
  if (y_coordinate < MARGIN + TITLE_HEIGHT) {
    return 0;
  }

  y_coordinate = y_coordinate - (MARGIN + TITLE_HEIGHT);
  return 1 + (y_coordinate / LINE_HEIGHT);
}

void CurrentTouchScreen::printLine(String label, String value, int lineNumber) {
  int top = TITLE_HEIGHT + (LINE_HEIGHT * lineNumber);

  tft.setTextColor(WHITE);
  tft.setTextSize(2);
  
  tft.setCursor(MARGIN, top);
  tft.print(label);
  
  tft.setCursor(LINE_X_START, top);
  tft.fillRect(LINE_X_START, top, X_SIZE - LINE_X_START, LINE_X_HEIGHT, BLACK);
  tft.print(value);
}

void CurrentTouchScreen::clearLine(int lineNumber) {
  int top = TITLE_HEIGHT + (LINE_HEIGHT * lineNumber);
  tft.setCursor(LINE_X_START, top);
  tft.fillRect(0, top, X_SIZE, LINE_X_HEIGHT, BLACK);
}

void CurrentTouchScreen::plot(float value) {
  Serial.println("CurrentTouchScreen::plot: " + String(value));
  
  int top = Y_SIZE - 100 * value;
  int left = (int) ((millis() / 1000) % X_SIZE);
  Serial.println("CurrentTouchScreen::plot::top " + String(top));
  Serial.println("CurrentTouchScreen::plot::left " + String(left));

  tft.drawPixel(left, top, WHITE);
  
}
