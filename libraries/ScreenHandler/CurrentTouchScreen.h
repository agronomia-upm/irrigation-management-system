/*
  CurrentTouchScreen.h - Library for managing TFT LCD Shield Touch Screen coordinates.
  Created by Samuel Barrado Aguirre.
  Released into the public domain.
*/
#ifndef CurrentTouchScreen_h
#define CurrentTouchScreen_h

#include "Arduino.h"

#include <TouchScreen.h>

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library

#define YP A2  // must be an analog pin, use "An" notation!
#define XM A3  // must be an analog pin, use "An" notation!
#define YM 8   // can be a digital pin
#define XP 9   // can be a digital pin

#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

#define TITLE_HEIGHT        25
#define LINE_HEIGHT         25
#define MARGIN              10

// This class abstracts the current touch screen
class CurrentTouchScreen {
  public:
    int x, y = 0 ;
    boolean clicked = false;

    void begin();
    void loop();

    // Clears screen
    void clear();

    // Writes title to screen.
    void title(String title);

    // Prints a line with format %label: %value
    void printLine(String label, String value, int lineNumber);
    void clearLine(int lineNumber) ;


    // Prints a dot in bottom chart
    void plot(float value);
    void axis();

    int getLine() ;

  private:
    // For better pressure precision, we need to know the resistance
    // between X+ and X- Use any multimeter to read it
    // For the one we're using, its 300 ohms across the X plate
    TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

    // If using the shield, all control and data lines are fixed, and
    // a simpler declaration can optionally be used:
    Adafruit_TFTLCD tft = Adafruit_TFTLCD(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
};

#endif
