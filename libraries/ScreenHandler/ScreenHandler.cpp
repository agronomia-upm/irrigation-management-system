#include "Arduino.h"
#include "ScreenHandler.h"


ScreenHandler::ScreenHandler(DataWarehouse *_data): BaseScreenHandler() {
  data = _data;
  ts = new CurrentTouchScreen();

  mainScreen = new MainScreen(_data, ts, this);
  valveScreen = new ValveScreen(_data, ts, this);
  filterScreen = new FilterScreen(_data, ts, this);
  flowScreen = new FlowScreen(_data, ts, this);
  dhtScreen = new DHTScreen(_data, ts, this);

  changeScreen(ScreenHandlerWindows::SCREEN_INDEX);
}

void ScreenHandler::changeScreen(ScreenHandlerWindows desiredScreen) {
  switch (desiredScreen) {
    case ScreenHandlerWindows::SCREEN_INDEX: screen = mainScreen; break;
    case ScreenHandlerWindows::SCREEN_VALVE: screen = valveScreen; break;
    case ScreenHandlerWindows::SCREEN_FILTER: screen = filterScreen; break;
    case ScreenHandlerWindows::SCREEN_FLOW: screen = flowScreen; break;
    case ScreenHandlerWindows::SCREEN_DHT: screen = dhtScreen; break;
  }

  screen->redraw();
}

void ScreenHandler::begin() {
  Serial.println("ScreenHandler::begin");
  ts->begin();
  screen->redraw();
}

void ScreenHandler::loop() {
  ts->loop();
  screen->loop();

  if (data->dirty()) {
    screen->refresh();
  }

  if (ts->clicked) {
    Serial.print("ts->clicked: ");
    int line = ts->getLine();


    screen->click(line);
    ts->clicked = false;
  }
}
