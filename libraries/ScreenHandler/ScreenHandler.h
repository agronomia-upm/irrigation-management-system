/*
  CurrentTouchScreen.h - Library for managing TFT LCD Shield Touch Screen coordinates.
  Created by Samuel Barrado Aguirre.
  Released into the public domain.
*/
#ifndef ScreenHandler_h
#define ScreenHandler_h

#include "Arduino.h"
#include "CurrentTouchScreen.h"
#include "DataWarehouse.h"

#include "Screen.h"
#include "MainScreen.h"
#include "ValveScreen.h"
#include "FilterScreen.h"
#include "FlowScreen.h"
#include "DHTScreen.h"
#include "BaseScreenHandler.h"


class ScreenHandler: public BaseScreenHandler {
  public:
    ScreenHandler(DataWarehouse *data);

    virtual void changeScreen(ScreenHandlerWindows desiredScreen) ;

    void begin();
    void loop();

  private:
    float state = 0;
    float lastMillis = 0;

    Screen *screen;
    MainScreen *mainScreen;
    FilterScreen *filterScreen;
    FlowScreen *flowScreen;
    ValveScreen *valveScreen;
    DHTScreen *dhtScreen;

    CurrentTouchScreen *ts;
    DataWarehouse *data;

};

#endif
