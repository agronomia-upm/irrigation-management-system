#include <Arduino.h>
#include <Wire.h>

#include "BusSlave.h"
#include "FlowHandler.h"
#include "PressureHandler.h"

#define FILTER_ADDR   9

#define FILTER1_PIN   A0
#define FILTER2_PIN   A1

PressureHandler pressure1   = PressureHandler(FILTER1_PIN);
PressureHandler pressure2   = PressureHandler(FILTER2_PIN);
long            nextMillis  = 0;

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Serial.println("FilterController::requestEvent()");
  BusSlave::send(pressure1.getPressure(), pressure2.getPressure());
}

void setup() {
  Serial.begin(9600);
  Wire.begin(FILTER_ADDR);            // join i2c bus with address #8
  Wire.onRequest(requestEvent);       // register event
}

void loop() {
  pressure1.loop();
  pressure2.loop();

  long currentMillis = millis();
  if (nextMillis < currentMillis) {
    nextMillis = (currentMillis + 1000);
    Serial.println("FilterController::loop()::press 1: " + String(pressure1.getPressure()));
    Serial.println("FilterController::loop()::press 2: " + String(pressure2.getPressure()));
  }
}
