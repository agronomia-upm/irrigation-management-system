/*
 * https://electricnoodlebox.wordpress.com/tutorials/esp32-ttgo-dev-board-with-oled-display-tutorial/
 * https://github.com/G6EJD/ESP32-D1-Mini-Format-Examples/blob/master/ESP32_WEMOS_OLED_D1_variant.ino.ino
 */
#include <WiFi.h>

#include <ArduinoJson.h>
#include <PubSubClient.h>

#define WIFI_SSID                 "agronomia"
#define WIFI_PASSWORD             "4gr0n0m14"
#define SERVER_URL                "192.168.4.1"


#define TOKEN                     "w8RRN0UkCrj8vuBGhxiu"

// =======================================

WiFiClient wifiClient;
PubSubClient client(wifiClient);

int status = WL_IDLE_STATUS;

// =======================================

void setup() {
  Serial.begin(9600);

  InitWiFi();
  client.setServer(SERVER_URL, 1883);
  client.setCallback(on_message);
}

// =======================================
long nextMillis = 0;

void loop() {
  if ( !client.connected() ) {
    reconnect();
  }

  long currentMillis = millis();
  if (nextMillis < currentMillis) {
    nextMillis = (currentMillis + 1000);

    String message = "{\"manometer4\":" + String(3 + getRandom()) + "}";
    client.publish("v1/devices/me/telemetry", message.c_str());
    Serial.println("Preassure sent:" + message);
  }

  client.loop();
}

// =======================================

// The callback for when a PUBLISH message is received from the server.
void on_message(const char* topic, byte* payload, unsigned int length) {

  Serial.println("On message");
  Serial.println(topic);
  Serial.println((char *) payload);
  Serial.println(length);

  String responseTopic = String(topic);
  responseTopic.replace("request", "response");

  //client.publish(responseTopic.c_str(), getStatusPayload().c_str());

}

// =======================================

void InitWiFi() {
  Serial.println("Connecting to AP ...");
  // attempt to connect to WiFi network

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}

// =======================================

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to ThingsBoard node ...");
    // Attempt to connect (clientId, username, password)
    if ( client.connect("Line end manometer #0", TOKEN, NULL) ) {
      Serial.println( "[DONE]" );
      // Subscribing to receive RPC requests
      client.subscribe("v1/devices/me/rpc/request/+");

      // Sending current GPIO status
      Serial.println("Sending current status ...");
      // client.publish("v1/devices/me/attributes", getStatusPayload().c_str());
    } else {
      Serial.print( "[FAILED] [ rc = " );
      Serial.print( client.state() );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}

// =======================================

float getRandom() {
  return random(1, 100) / 100.0 ;
}
