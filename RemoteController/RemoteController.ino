/**
   This module forwards the information received on a serial port to ThingsBoard platform.

   Information received must be formatted as a valid JSON because this module is data agnostic
   as it ignore forwarded info.
*/


#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <SoftwareSerial.h>

#define WIFI_SSID                 "agronomia"
#define WIFI_PASSWORD             "4gr0n0m14"
#define SERVER_URL                "192.168.4.1"

#define QUEUE_TOKEN               "dXebCjFj0OGDDKQxl9zY"
#define QUEUE_DEVICE_LABEL        "Irrigation header #0"


WiFiClient wifiClient;
PubSubClient queueClient(wifiClient);

int status = WL_IDLE_STATUS;

/**
   Initial setup for RemoteController module.
*/
void setup() {
  //
  Serial.begin(9600);
  
  initWiFi();

  queueClient.setServer(SERVER_URL, 1883);
  queueClient.setCallback(on_message);
}

/**
   Main loop.

   It handles wifi connection and the information forwarding from
   serial connected device to thingsboard platform.
*/
void loop() {
  if ( !queueClient.connected() ) {
    reconnectQueue();
  }

  if (Serial.available()) {
    String received = Serial.readString();
    queueClient.publish("v1/devices/me/telemetry", received.c_str());
  }

  queueClient.loop();
}

/**
    The callback func when a PUBLISH message is received from the server.
*/
void on_message(const char* topic, byte* payload, unsigned int length) {
  // First of all, answer platform request.
  String responseTopic = String(topic);
  String responsePayload = "";
  responseTopic.replace("request", "response");
  queueClient.publish(responseTopic.c_str(), responsePayload.c_str());

  // Next, forward message to BusMaster
  // TODO: Forward message to BusMaster
}

/**
   connects to WiFi network
*/
void initWiFi() {
  // Serial.println("Connecting to AP ...");

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    // Serial.print(".");
  }
  // Serial.print("Connected to AP");
  // Serial.println("IP address: ");
  // Serial.println(WiFi.localIP());
}

/**
   [Re]connects to the MQTT queue from thingsboard.
*/
void reconnectQueue() {
  // Loop until we're reconnected
  while (!queueClient.connected()) {

    if ( WiFi.status() != WL_CONNECTED) {
      initWiFi();
    }

    // Serial.print("Connecting to ThingsBoard node ...");
    // Attempt to connect (clientId, username, password)
    if ( queueClient.connect(QUEUE_DEVICE_LABEL, QUEUE_TOKEN, NULL) ) {
      // Subscribing to receive RPC requests
      queueClient.subscribe("v1/devices/me/rpc/request/+");
      // Serial.println( "[DONE]" );
    }
    else {
      // Serial.print( "[FAILED] [ rc = " );
      // Serial.print( queueClient.state() );
      // Serial.println( " : retrying in 5 seconds]" );
      delay( 5000 );
    }
  }
}
