#include <Wire.h>
#include <Arduino.h>

#include "RelayHandler.h"
#include "BusSlave.h"

#define RELAY_PIN 8
#define VALVE_ADDR 8

RelayHandler *relay = new RelayHandler(RELAY_PIN);

/**
   function that executes whenever data is requested by master
   this function is registered as an event, see setup()
*/
void requestEvent() {
  Serial.println("ValveController::requestEvent()");
  float state = relay->get();
  BusSlave::send(state);
}

/**
  function that executes whenever data is received from master.
  This function is registered as an event, see setup()
*/
void receiveEvent(int howMany) {

  int x = BusSlave::read();    // receive byte as an integer
    Serial.println("ValveController::receiveEvent(): " + String(x));


  switch (x) {
    case ValveStates::Open:
      relay->set(ValveStates::Open);
      Serial.println("ValveController::receiveEvent(howMany)::OPEN");
      break;

    case ValveStates::Close:
      relay->set(ValveStates::Close);
      Serial.println("ValveController::receiveEvent(howMany)::CLOSED");
      break;
  }
}


void setup() {
  Serial.begin(9600);

  relay->begin();

  Wire.begin(VALVE_ADDR);
  Wire.onRequest(requestEvent); // register event
  Wire.onReceive(receiveEvent); // register event
}

long nextMillis = 0;

void loop() {
  relay->loop();

  long currentMillis = millis();
  if (nextMillis < currentMillis) {
    nextMillis = (currentMillis + 10000);

    Serial.println("ValveController::loop()::relay.toggle()");
    relay->toggle();
  }
}
